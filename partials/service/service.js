(function () {
    var controllerId = "bookService";
    angular.module("app").controller(controllerId, ['$scope', 'configService', bookService]);

    function bookService($scope, configService) {
        $scope.store = configService;
        var vm = this;
        vm.service = {
            early_date: "",
            late_date: "",
            regNo: "",
            make: "",
            model: "",
            vin: "",
            firstReg: "",
            km: "",
            company: "",
            title: "",
            fName: "",
            lName: "",
            mobile: "",
            landline: "",
            email: "",
            isCustomer: "",
            serviceType: "",
            payMethod: ""
        };

        function spinner() {
            document.getElementById("submit").addEventListener("click", function () {
                angular.element("#spinner").css("display", "block");
            });
        }

        spinner();
        vm.sendMail = function sendEmail() {
            var serviceRequest = "";
            var data = {
                notification: {
                    ToList: "info@hyundaiworcester.co.za",//$scope.store.eMail.carServiceEmail
                    CcList: "",
                    BccList: "webmaster@vmgsoftware.co.za",
                    Subject: "Service reguest from " + vm.service.name,
                    Message: "I would like to book a service for:"
                    + "\n\nEarliest Date: " + vm.service.early_date
                    + "\nLatest Date: " + vm.service.late_date
                    + "\nRegistration Number: " + vm.service.regNo
                    + "\nMake: " + vm.service.make
                    + "\nModel: " + vm.service.model
                    + "\nVin Number (Placed in Windscreen Corner): " + vm.service.vin
                    + "\nYear of 1st registration: " + vm.service.firstReg
                    + "\nKilometres: " + vm.service.km
                    + "\nCompany: " + vm.service.company
                    + "\nTitle: " + vm.service.title
                    + "\nFirst Name: " + vm.service.fName
                    + "\nSurname: " + vm.service.lName
                    + "\nCellphone: " + vm.service.mobile
                    + "\nPhone: " + vm.service.landline
                    + "\nEmail: " + vm.service.email
                    + "\nI am an existing customer: " + vm.service.isCustomer
                    + "\nService Type: " + vm.service.serviceType
                    + "\nPayment method: " + vm.service.payMethod,
                    MessageSubject: "Service reguest from " + vm.service.name,
                    ToName: $scope.store.eMail.carServiceEmailTo,
                    FromName: vm.service.name,
                    FromEmail: vm.service.email
                }
            };
            serviceRequest = JSON.stringify(data);
            $.ajax({
                url: "http://www.vmgsoftware.co.za/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmail",
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    accept: "application/json"
                },
                data: serviceRequest,
                dataType: "json",
                success: function (response) {
                    document.getElementById("serviceForm").reset();
                    angular.element("#serviceForm").css("display", "none");
                    angular.element("#mailsent").css("display", "block");
                    angular.element("#spinner").css("display", "none");
                },
                error: function (response) {
                    document.getElementById("serviceForm").reset();
                    angular.element("#serviceForm").css("display", "none");
                    angular.element("#mailnotsent").css("display", "block");
                }
            });
        };
    }
})();