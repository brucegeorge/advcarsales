(function () {
    var controllerId = "bookPart";
    angular.module("app").controller(controllerId, ['$scope', 'configService', bookService]);

    function bookService($scope, configService) {
        $scope.store = configService;
        var vm = this;
        vm.part = {
            make: "",
            vin: "",
            fName: "",
            lName: "",
            mobile: "",
            message: "",
            email: ""
        };

        function spinner() {
            document.getElementById("submit").addEventListener("click", function () {
                angular.element("#spinner").css("display", "block");
            });
        }

        spinner();
        vm.sendMail = function sendEmail() {
            var partRequest = "";
            var data = {
                notification: {
                    ToList: "info@hyundaiworcester.co.za",//$scope.store.eMail.carServiceEmail
                    CcList: "",
                    BccList: "webmaster@vmgsoftware.co.za",
                    Subject: "Part reguest from " + vm.part.fName,
                    Message: vm.part.message
                    + "\n\nMake: " + vm.part.make
                    + "\nVin Number: " + vm.part.vin
                    + "\nFirst Name: " + vm.part.fName
                    + "\nSurname: " + vm.part.lName
                    + "\nCellphone: " + vm.part.mobile
                    + "\nEmail: " + vm.part.email,
                    MessageSubject: "Part reguest from " + vm.part.fName,
                    ToName: $scope.store.eMail.carServiceEmailTo,
                    FromName: vm.part.name,
                    FromEmail: vm.part.email
                }
            };
            partRequest = JSON.stringify(data);
            $.ajax({
                url: "http://www.vmgsoftware.co.za/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmail",
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    accept: "application/json"
                },
                data: partRequest,
                dataType: "json",
                success: function (response) {
                    document.getElementById("partForm").reset();
                    angular.element("#partForm").css("display", "none");
                    angular.element("#mailsent").css("display", "block");
                    angular.element("#spinner").css("display", "none");
                },
                error: function (response) {
                    document.getElementById("partForm").reset();
                    angular.element("#partForm").css("display", "none");
                    angular.element("#mailnotsent").css("display", "block");
                }
            });
        };
    }
})();