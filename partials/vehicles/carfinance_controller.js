(function () {
    var controllerId = "carFinance";
    angular.module("app")
       .controller(controllerId, ['$scope', 'configService', '$state', finApp]);

    function finApp($scope, configService) {
        $scope.store = configService;
        var vm = this;
        vm.modalFin = {
            name: "",
            lastname: "",
            idnumber: "",
            number: "",
            email: "",
            message: "",
            permission: "",
            deposit: "",
            depositprice: "",
            tradein: "",
            tradeindetails: "",
            companyid: "",
            stock_id: "",
            stock_code: "",
            make: "",
            series: "",
            year: "",
            dealer_email: "",
            dealer_name: "",
            selling_price: ""
        };

        function spinner() {
            document.getElementById("submitbut").addEventListener("click", function () {
                angular.element("#spinner").show();
            });
        }

        spinner();
        vm.sendMail = function sendEmail() {
            var financeApp = "";
            var data = {
                notification: {
                    ToList: $scope.store.eMail.carFinanceEmail,
                    CcList: vm.modalFin.email,
                    BccList: "webmaster@vmgsoftware.co.za",
                    Subject: $scope.store.eMail.carFinanceEmailTo + " Finance Application from " + vm.modalFin.name + ", with regards to: " + vm.modalFin.make + " " + vm.modalFin.series,
                    Message: vm.modalFin.message
                    + "\n\nIdentity or Passport Number: " + vm.modalFin.idnumber
                    + "\nContact Number: " + vm.modalFin.number
                    + "\nStock Code: " + vm.modalFin.stock_code
                    + "\nMake: " + vm.modalFin.make
                    + "\nModel: " + vm.modalFin.series
                    + "\nYear: " + vm.modalFin.year
                    + "\nPrice: " + vm.modalFin.selling_price
                    + "\nNCR Credit Check: " + vm.modalFin.permission
                    + "\nDeposit: " + vm.modalFin.deposit
                    + "\nDeposit Amount: " + vm.modalFin.depositprice
                    + "\nTrade In: " + vm.modalFin.tradein
                    + "\nTrade In Details: " + vm.modalFin.tradeindetails,
                    MessageSubject: $scope.store.eMail.carFinanceEmailTo + " Finance Application from " + vm.modalFin.name,
                    ToName: $scope.store.eMail.carFinanceEmailTo,
                    FromName: vm.modalFin.name + ' ' + vm.modalFin.lastname,
                    FromEmail: vm.modalFin.email
                }
            };
            financeApp = JSON.stringify(data);
            $.ajax({
                url: "http://www.vmgsoftware.co.za/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmail",
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    accept: "application/json"
                },
                data: financeApp,
                dataType: "json",
                success: function (response) {
                    document.getElementById("finAppForm").reset();
                    angular.element("#finAppForm").hide();
                    angular.element("#mailsent").show();
                    angular.element("#spinner").hide();
                },
                error: function (response) {
                    document.getElementById("finAppForm").reset();
                    angular.element("#finAppForm").hide();
                    angular.element("#mailnotsent").show();
                }
            });
        };
    }
})();