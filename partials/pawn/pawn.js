(function () {
    var controllerId = "pawnCar";
    angular.module("app").controller(controllerId, ['$scope', 'configService', contactUs]);

    function contactUs($scope, configService) {
        $scope.store = configService;
        var vm = this;
        vm.details = {
            name: "",
            number: "",
            email: "",
            message: "",
            own: "",
            certificate: "",
            debt: "",
            registered: "",
            amount: "",
            make: "",
            year: "",
            km: "",
            accident: "",
            accident_report: "",
            paid: ""
        };

        function spinner() {
            document.getElementById("submit").addEventListener("click", function () {
                angular.element("#spinner").css("display", "block");
            });
        }

        spinner();
        $scope.own = "No";
        $scope.certificate = "No";
        $scope.registered = "No";
        $scope.debt = "No";
        $scope.accident = "No";
        $scope.paid = "No";
        $scope.ownChange = function () {
            $scope.own = (vm.details.own) ? "Yes" : "No";
        };
        $scope.certificateChange = function () {
            $scope.certificate = (vm.details.certificate) ? "Yes" : "No";
        };
        $scope.debtChange = function () {
            $scope.debt = (vm.details.debt) ? "Yes" : "No";
        };
        $scope.registeredChange = function () {
            $scope.registered = (vm.details.registered) ? "Yes" : "No";
        };
        $scope.accidentChange = function () {
            $scope.accident = (vm.details.accident) ? "Yes" : "No";
        };
        $scope.paidChange = function () {
            $scope.paid = (vm.details.paid) ? "Yes" : "No";
        };
        vm.sendMail = function sendEmail() {
            var contactRequest = "";
            var data = {
                notification: {
                    ToList: $scope.store.eMail.contactEmail,//$scope.store.eMail.contactEmail
                    CcList: "",
                    BccList: "webmaster@vmgsoftware.co.za",
                    Subject: "Pawn request from " + vm.details.name,
                    Message: vm.details.message +
                    "\n\nContact me on: " + vm.details.number +
                    "\nDo you own your vehicle / asset outright: " + $scope.own +
                    "\nAre you in possession of the original vehicle registration certificate: " + $scope.certificate +
                    "\nIs your vehicle / asset fully paid and free from any debt: " + $scope.debt +
                    "\nIs the vehicle / asset registered in your name: " + $scope.registered +
                    "\n\nASSET DETAILS" +
                    "\nLoan Amount: R" + vm.details.amount +
                    "\nMake: " + vm.details.make +
                    "\nYear: " + vm.details.year +
                    "\nKM: " + vm.details.km +
                    "\nHas the vehicle been in an accident: " + $scope.accident +
                    "\nAccident Report: " + vm.details.accident_report +
                    "\n Is the vehicle / asset fully paid for: " + $scope.paid,
                    MessageSubject: "Pawn request from " + vm.details.name,
                    ToName: $scope.store.eMail.contactEmailTo,
                    FromName: vm.details.name,
                    FromEmail: vm.details.email
                }
            };
            contactRequest = JSON.stringify(data);
            $.ajax({
                url: "http://www.vmgsoftware.co.za/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmail",
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    accept: "application/json"
                },
                data: contactRequest,
                dataType: "json",
                success: function (response) {
                    document.getElementById("pawnForm").reset();
                    angular.element("#pawnForm").css("display", "none");
                    angular.element("#mailsent").css("display", "block");
                    angular.element("#spinner").css("display", "none");
                },
                error: function (response) {
                    document.getElementById("pawnForm").reset();
                    angular.element("#pawnForm").css("display", "none");
                    angular.element("#mailnotsent").css("display", "block");
                }
            });
        };
    }
})();