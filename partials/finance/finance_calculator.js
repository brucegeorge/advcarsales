(function () {
    'use strict';
    // Controller name is handy for logging
    // Define the controller on the module.
    // Inject the dependencies.
    // Point to the controller definition function.
    angular.module('app')
       .controller("financeCalculator", [financeCalculator]);

    function financeCalculator($routeParams) {
        // Using 'Controller As' syntax, so we assign this to the vm variable (for viewmodel).
        var vm                         = this;
        vm.licence                     = 0;
        vm.service                     = 0;
        // Bindable properties and functions are placed on vm.
        vm.title                       = 'Finance Calculator';
        vm.calculate                   = calculate;
        vm.calculateTotalLoan          = calculateTotalLoan;
        vm.calculateExtraMoney         = calculateExtraMoney;
        vm.calculateResidualPercentage = calculateResidualPercentage;
        vm.calculateResidualCash       = calculateResidualCash;

        function calcServices() {
            return vm.licence + vm.service;
        }

        vm.calculator                 = {
            typesOfBuyer           : [
                {name: 'individual', text: 'an individual'},
                {name: 'allowance', text: 'a car allowance buyer'},
                {name: 'company', text: 'a company'}
            ],
            vehiclePrice           : "",
            deposit                : "",
            tradeIn                : "",
            totalLoanAmount        : "",
            repaymentPeriods       : [6, 12, 24, 36, 42, 48, 54, 60, 72],
            residualPercent        : "",
            residualCash           : "",
            totalMonthlyInstallment: "",
            licence                 : vm.licence,
            service                 : vm.service
        };
        vm.calculator.buyerType       = vm.calculator.typesOfBuyer[1];
        vm.calculator.repaymentPeriod = 48;
        vm.extraMoney                 = {
            payment    : 0,
            reducedTerm: 0,
            save       : 0
        };
        initialiseInterestRates();

        function calculate(inputName) {
            calculateTotalLoan();
            switch (inputName) {
            case 'cash':
                calculateResidualPercentage();
                break;
            case 'percent':
                calculateResidualCash();
                break;
            default:
                calculateResidualCash();
                break;
            }
            // validate form
            if (vm.calculator.totalLoanAmount === 0)
                return;
            var payments                          = calculateMonthlyInstallment(vm.calculator.interestRate, vm.calculator.repaymentPeriod,
               vm.calculator.residualPercent, vm.calculator.vehiclePrice, vm.calculator.totalLoanAmount, vm.calculator.licence, vm.calculator.service);
            vm.calculator.totalMonthlyInstallment = parseFloat(formatString(payments));
            if (vm.extraMoney.payment > 0) {
                calculateExtraMoney();
            }
        }

        function calculateTotalLoan() {
            var totalDeduction = vm.calculator.deposit + vm.calculator.tradeIn;
            if (totalDeduction >= vm.calculator.vehiclePrice) {
                if (vm.calculator.vehiclePrice > 0) {
                    vm.calculator.totalLoanAmount = 0;
                    alert('The deposit plus trade in value is more than the new price of the vehicle, you don\'t need finance');
                }
            }
            else {
                vm.calculator.totalLoanAmount = vm.calculator.vehiclePrice + calcServices() - totalDeduction;
            }
        }

        function calculateExtraMoney() {
            if (vm.calculator.totalMonthlyInstallment === 0) {
                alert('First complete the calculator section.');
                vm.extraMoney.payment = 0;
                return;
            }
            var newTerm               = calculateRepaymentPeriod(
               vm.calculator.interestRate,
               vm.calculator.residualPercent,
               vm.calculator.vehiclePrice,
               vm.calculator.totalLoanAmount,
               vm.calculator.totalMonthlyInstallment,
               vm.extraMoney.payment);
            vm.extraMoney.reducedTerm = Math.round(newTerm);
            var interest              = calculateInterest(
               vm.calculator.totalLoanAmount,
               vm.calculator.interestRate,
               vm.calculator.repaymentPeriod,
               vm.calculator.totalMonthlyInstallment,
               0);
            var interestWithExtra     = calculateInterest(
               vm.calculator.totalLoanAmount,
               vm.calculator.interestRate,
               vm.extraMoney.reducedTerm,
               vm.calculator.totalMonthlyInstallment,
               vm.extraMoney.payment);
            var interestDifference    = interest - interestWithExtra;
            vm.extraMoney.save        = parseFloat(formatString(interestDifference));
        }

        function calculateResidualCash() {
            if (vm.calculator.buyerType.name === 'individual') {
                if ((vm.calculator.residualPercent > 0) ||
                   (vm.calculator.residualCash > 0)) {
                    alert('No residual allowed for individuals');
                    clearTotals();
                }
                return;
            }
            if (vm.calculator.totalLoanAmount === 0) {
                clearTotals();
                return;
            }
            vm.calculator.residualCash =
               parseFloat(formatString(
                  vm.calculator.totalLoanAmount * (vm.calculator.residualPercent / 100.0)
               ));
        }

        function clearTotals() {
            vm.calculator.residualCash            = 0;
            vm.calculator.residualPercent         = 0;
            vm.calculator.totalMonthlyInstallment = 0;
        }

        function calculateResidualPercentage() {
            if (vm.calculator.buyerType.name === 'individual') {
                alert('No residual allowed for individuals');
                clearTotals();
                return;
            }
            if (vm.calculator.totalLoanAmount === 0) {
                clearTotals();
                return;
            }
            vm.calculator.residualPercent =
               (vm.calculator.residualCash / vm.calculator.totalLoanAmount) * 100.0;
        }

        //#region Internal Methods
        function initialiseInterestRates() {
            vm.calculator.interestRates = [];
            for (var i = 9.0; i <= 20.0; i += 0.25) {
                vm.calculator.interestRates.push(i);
            }
            vm.calculator.interestRate = 15;
        }

        function calculateMonthlyInstallment(interestRate, repaymentPeriod, residualPercent, vehiclePrice, totalLoanAmount) {
            var interest = (interestRate / 100.0) / 12;
            var power    = 1;
            for (var j = 0; j < repaymentPeriod; j++)
                power = power * (1 + interest);
            var residualAmount  = (residualPercent * vehiclePrice) / 100;
            var finalLoanAmount = totalLoanAmount - (residualAmount / power);
            var paymentPerMonth = (finalLoanAmount * power * interest) / (power - 1);
            return paymentPerMonth;
        }

        function calculateRepaymentPeriod(interestRate, residualPercent, vehiclePrice, totalLoanAmount, totalMonthlyInstallment, extraPayment) {
            var interest        = (interestRate / 100.0) / 12;
            var residualAmount  = (residualPercent * vehiclePrice) / 100;
            var total           = totalLoanAmount - residualAmount;
            var paymentPerMonth = totalMonthlyInstallment + extraPayment;
            var interestCount   = 0;
            var totalCount      = 0;
            var count           = 0;
            while (Number(total) > Number(paymentPerMonth)) {
                interestCount = total * interest;
                totalCount    = Number(paymentPerMonth) - Number(interestCount);
                total         = Number(total) - Number(totalCount);
                count         = Number(count) + Number(1);
            }
            var repaymentTerm = count;
            var paymentPart   = parseInt(total / paymentPerMonth * 100, 10);
            return repaymentTerm + '.' + paymentPart;
        }

        function calculateInterest(total, interestRate, paymentPeriod, monthlyPayment, extraPayment) {
            var interest        = (interestRate / 100) / 12;
            var interestCount   = 0;
            var monthlyInterest = 0;
            var totalBalance    = total;
            for (var i = 0; i < paymentPeriod; i++) {
                monthlyInterest = interest * totalBalance;
                interestCount += monthlyInterest;
                totalBalance -= ((monthlyPayment + extraPayment) - monthlyInterest); // subtract monthly principal
            }
            return interestCount;
        }

        function formatString(number) {
            number           = Math.round(number * 100) / 100 + 0.0001;
            var numberString = "" + number;
            var index        = numberString.lastIndexOf(".");
            number           = numberString.substring(0, index + 3);
            if (number == 0.00) {
                return 0;
            }
            else {
                return number;
            }
        }

        //#endregion
    }
})();
