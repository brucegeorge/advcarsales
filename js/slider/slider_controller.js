(function () {
    angular.module("app")
       .controller("slider", ["$scope", function ($scope) {
           $scope.slides = [
               {
                   url: "img/slides/s1a.jpg",
                   text1: "Welcome to",
                   text1a: "ADV Cars",
                   text2: "TOP QUALITY CARS, TOP QUALITY SERVICE!",
                   text4: "Ubi est clemens mineralis? A falsis, diatria festus idoleum. Mortems sunt hippotoxotas de noster castor. Clemens, pius hippotoxotas virtualiter captis de germanus, lotus animalis."
               },
               {
                   url: "img/slides/s4a.jpg",
                   text1: "Welcome to",
                   text1a: "ADV Cars",
                   text3: "...exceptional pre-owned vehicles",
                   text4: "Cum lanista peregrinationes, omnes genetrixes reperire germanus, brevis parmaes. Eheu, bassus absolutio! Advenas accelerare in divio! Sagas sunt rumors de alter pulchritudine. Cum hippotoxota ridetis, omnes navises resuscitabo peritus, bassus fiscinaes."
               },
               {
                   url: "img/slides/s3a.jpg",
                   text1: "Welcome to",
                   text1a: "ADV Cars",
                   text2: "",
                   text4: "Sunt fluctuses aperto teres, audax bullaes. Clemens torus semper anhelares solem est. Cur vigil resistere? Lotus species sed mire imitaris valebat est. Extum de noster mensa, carpseris adiurator."
               },
               {
                   url: "img/slides/s5a.jpg",
                   text1: "Welcome to",
                   text1a: "ADV Cars",
                   text2: "",
                   text4: "Heu, grandis tus! Rationes sunt vortexs de varius imber. A falsis, silva secundus mens. Scutum, spatii, et extum. Grandis, nobilis abnobas interdum desiderium de varius, germanus adelphis."
               },
               {
                   url: "img/slides/s2a.jpg",
                   text1: "Welcome to",
                   text1a: "ADV Cars",
                   text3: "...exceptional pre-owned vehicles",
                   text4: "A falsis, demissio dexter fortis. A falsis, palus teres castor. Persuadere tandem ducunt ad flavum frondator. Cur abactor persuadere? Est peritus demissio, cesaris. Cum finis messis, omnes liberies examinare nobilis, velox cedriumes."
               }
           ];
           $scope.sliderOptions = {
               controlNav: false,
               animationLoop: true,
           }
       }
       ])
})();