(function () {
    var app = angular.module("app");
    app.factory('configService', function () {
        var store = {
            name: "ADV Cars",
            branch1: "ADV Cars",
            email1: "info@ADV Cars.co.za",
            facebookUrl: "",
            instaUrl: " ",
            twitterUrl: "",
            googleplusUrl: " ",
            youtubeUrl: "",
            address1: "128 Alumina Allee, Alton, Richardsbay",
            tel1: "035 751 2348",
            tel2: "",
            tel3: "",
            cell1: "",
            fax1: "035 751 2382",
            eMail: {
                contactEmail: "officemanager.rbay@kspgroup.co.za",
                contactEmailTo: "ADV Cars",
                carRequestEmail: "officemanager.rbay@kspgroup.co.za",
                carRequestEmailTo: "ADV Cars",
                carSellEmail: "officemanager.rbay@kspgroup.co.za",
                carSellEmailTo: "ADV Cars",
                carServiceEmail: "officemanager.rbay@kspgroup.co.za",
                carServiceEmailTo: "ADV Cars",
                carFinanceEmail: "officemanager.rbay@kspgroup.co.za",
                carFinanceEmailTo: "ADV Cars",
                carEnquireEmail: "francois@ADV Cars.co.za",
                carEnquireEmailTo: "ADV Cars",
                testimonialEmail: "officemanager.rbay@kspgroup.co.za",
                testimonialEmailTo: "ADV Cars",
                bccEmail: "webmaster@vmgsoftware.co.za",
                ccEmail: ""
            },
            tradinghours: {
                monFri: '07:30 - 17:30',
                sat: '08:00 – 13:00',
                sun: 'Closed',
                pub: '08:00 - 13:00'
            },
            latlong: '-29.723298, 31.063959',
            latlong1: '',
            mapLink: "https://www.google.co.za/maps/place/128+Alumina+Allee+St,+Alton,+Richards+Bay/@-28.73661,32.0357613,17z/data=!3m1!4b1!4m5!3m4!1s0x1efa3d2a80ca05f3:0x7d5cde7d672fa8f0!8m2!3d-28.73661!4d32.03795",
            mapLinkbranch1: "https://www.google.co.za/maps/place/128+Alumina+Allee+St,+Alton,+Richards+Bay/@-28.73661,32.0357613,17z/data=!3m1!4b1!4m5!3m4!1s0x1efa3d2a80ca05f3:0x7d5cde7d672fa8f0!8m2!3d-28.73661!4d32.03795",
            mapLinkShortLink1: ""
        };
        return store;
    });
})();